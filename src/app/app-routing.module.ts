import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ViewCareerInfoComponent } from '@pages/career/view/v-career-info.component';
import { ViewTechnologiesComponent } from '@pages/technologies/view/v-technologies.component';
import { ViewUserInfoComponent } from '@pages/user-info/view/v-user-info.component';

const routes: Routes = [
  { path: '', redirectTo: '/user-info', pathMatch: 'full' },
  { path: 'user-info', component: ViewUserInfoComponent },
  { path: 'career-info', component: ViewCareerInfoComponent },
  { path: 'technologies', component: ViewTechnologiesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
