export interface FirebaseResponse {
  img: string;
  message: string;
  nickname: string;
  position: string;
}
