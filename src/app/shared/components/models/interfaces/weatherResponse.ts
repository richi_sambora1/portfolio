export interface WeatherResponse {
  weather: object;
  main: object;
  wind: object;
  clouds: object;
  sys: object;
  name: string;
  id: number;
  timezone: number;
  cod: number;
}
