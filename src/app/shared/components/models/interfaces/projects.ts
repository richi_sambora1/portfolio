export interface Projects {
  img: string;
  githubUrl: string;
  description: string[];
}
