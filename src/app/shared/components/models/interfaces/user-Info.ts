export interface UserInfo {
  fullName: string;
  position: string;
  dateOfBirth: string;
  address: string;
}
