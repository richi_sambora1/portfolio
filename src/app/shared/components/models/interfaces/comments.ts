export interface Comments {
  nickname: string;
  message: string;
  position: string;
  img: string;
}
