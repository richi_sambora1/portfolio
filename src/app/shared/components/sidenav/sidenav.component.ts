import { Component } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent {
  public isCollapsed: boolean;
}
