import { SidenavComponent } from '@shared/components/sidenav/sidenav.component';
import { WeatherWidgetComponent } from '@shared/components/weather-widget/weather-widget.component';

export const SharedComponents = [
  SidenavComponent,
  WeatherWidgetComponent,
];
