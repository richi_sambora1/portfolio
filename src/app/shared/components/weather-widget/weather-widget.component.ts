import { Component, OnInit } from '@angular/core';
import { WeatherRequestService } from '@services/http-requests/weather-request/weather-request.service';

@Component({
  selector: 'weather-widget',
  templateUrl: './weather-widget.component.html',
  styleUrls: ['./weather-widget.component.scss'],
})
export class WeatherWidgetComponent implements OnInit {
  public weatherLocation: any;
  public weatherWind: any;
  public weatherTemp: any;
  public weatherTempFillsLike: any;

  constructor(
    public readonly weatherRequestService: WeatherRequestService,
  ) { }

  public ngOnInit(): void {
    this.weatherRequestService.getTimeRequest$()
      .subscribe(
        response => {
          this.weatherLocation = (response.name);
          this.weatherWind = JSON.stringify(response.wind['speed']);
          this.weatherTemp = JSON.stringify(response.main['temp']);
          this.weatherTempFillsLike = JSON.stringify(response.main['feels_like']);
        },
      );
  }
}
