export enum routingLinks {
  USER_INFO = '/user-info',
  CAREER = '/career-info',
  TECHNOLOGIES = '/technologies',
}
