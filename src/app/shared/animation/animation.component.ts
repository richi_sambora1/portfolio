import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';

@Component({
  selector: 'app-animation',
  templateUrl: './animation.component.html',
  styleUrls: ['./animation.component.scss'],
  animations: [
    trigger('expander', [
      state('initial', style({
        backgroundColor: 'green',
        width: '100px',
        height: '100px',
      })),
      state('expand', style({
        backgroundColor: 'blue',
        width: '200px',
        height: '200px',
      })),
      transition('initial=>expand', animate('1500ms')),
      transition('expand=>initial', animate('1000ms')),
    ]),
    trigger('hover', [
      state('initial', style({
        backgroundColor: '',
      })),
      state('onHover', style({
        backgroundColor: 'purple',
      })),
      transition('initial=>hover', animate('400ms')),
      transition('hover=>initial', animate('200ms')),
    ]),
  ],
})
export class AnimationComponent {
  public currentState = 'initial';

  public changeState(): void {
    this.currentState = this.currentState === 'initial' ? 'hover' : 'initial';
  }
}
