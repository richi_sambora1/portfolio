import { Technologies } from '@pages/technologies/section';
import { ViewTechnologiesComponent } from '@pages/technologies/view/v-technologies.component';

export const TechnologiesComponents = [
  Technologies,
  ViewTechnologiesComponent,
];
