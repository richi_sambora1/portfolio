import { Component, Input } from '@angular/core';

import { Techno } from '@shared/components/models/interfaces/technologies';

@Component({
  selector: 's-technologies',
  templateUrl: './s-technologies.component.html',
  styleUrls: ['./s-technologies.component.scss'],
})
export class TechnologiesComponent {
  @Input() public techno: Techno[];
}
