import { ViewCareerInfoComponent } from '@pages/career/view/v-career-info.component';
import { Career } from './section';

export const CareerComponents = [
  Career,
  ViewCareerInfoComponent,
];
