import { Component, Input } from '@angular/core';

import { Projects } from '@shared/components/models/interfaces/projects';

@Component({
  selector: 's-career-info',
  templateUrl: './s-career-info.component.html',
  styleUrls: ['./s-career-info.component.scss'],
})
export class CareerInfoComponent {
  @Input() public projects: Projects[];
}
