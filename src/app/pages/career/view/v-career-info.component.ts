import { Component } from '@angular/core';
import { Projects } from '@shared/components/models/interfaces/projects';

@Component({
  selector: 'v-career-info',
  templateUrl: './v-career-info.component.html',
})
export class ViewCareerInfoComponent {
  public projects: Projects[] = [
    {
      img: 'assets/img/project-photos/first_form.PNG',
      githubUrl: 'https://github.com/nikitaSufranovichJS/nikita_rep.git',
      description: ['Angular', 'Form module', 'Services', 'Local Storage'],
    },
    {
      img: 'assets/img/project-photos/Slack comunity group.PNG',
      githubUrl: 'https://bitbucket.org/richi_sambora1/training-tasks/src/master/',
      description: ['Angular', 'Services'],
    },
    {
      img: 'assets/img/project-photos/Delivery_1.PNG',
      githubUrl: 'https://bitbucket.org/richi_sambora1/work-tasks/src/sixteen-hour-task/',
      description: ['Angular', 'Angular Material', 'Material Table', 'Http Services', 'Form module', 'Services', 'Local Storage'],
    },
  ];
}
