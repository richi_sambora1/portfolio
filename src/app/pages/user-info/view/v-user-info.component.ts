import { Component, OnInit } from '@angular/core';
import { UserInfo } from '@shared/components/models/interfaces/user-Info';
import { AngularFireDatabase } from 'angularfire2/database';

import { MessagesService } from '@services/messages/messages.service';
import { Comments } from '@shared/components/models/interfaces/comments';
import { FirebaseResponse } from '@shared/components/models/interfaces/firebaseResponse';

@Component({
  selector: 'user-info',
  templateUrl: './v-user-info.component.html',
})

export class ViewUserInfoComponent implements OnInit {
  public firebaseComments;

  constructor(
    public readonly messagesService: MessagesService,
    public readonly db: AngularFireDatabase,
  ) {
  }

  public userMessages: Comments[] = [
    {
      nickname: 'Loreleya228',
      message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto et ut vel!',
      position: 'Bot',
      img: 'assets/img/profile-photos/profile_photo_1.png',
    },
    {
      nickname: 'SantaLuchiya1488',
      message: ' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci doloremque, dolorum ea eligendi eos esse facere in optio quidem quisquam quo recusandae, repellat ut. Amet commodi illum neque optio, quodsint tenetur?',
      position: 'Work man',
      img: 'assets/img/profile-photos/profile_photo_2.png',
    },
    {
      nickname: 'Ananas1337',
      message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium autem cumque cupiditate et eveniet ipsam iusto minima nesciunt quibusdam, sapiente tempore ullam voluptates.',
      position: 'Headmaster',
      img: 'assets/img/profile-photos/profile_photo_5.png',
    },
  ];
  public userInfo: UserInfo[] = [
    {
      fullName: 'Никита Суфранович',
      address: 'Петра Глебки 26',
      dateOfBirth: '22.03.1999',
      position: 'Pre-Junior Frontend Dev',
    },
  ];

  public addComment(): void {
    this.userMessages.push({
      nickname: this.messagesService.messageData.name,
      message: this.messagesService.messageData.text,
      position: this.messagesService.messageData.position,
      img: 'assets/img/profile-photos/profile_photo_3.png',
    });
  }

  public firebaseData(): void {
    this.db.list<FirebaseResponse>('/comments')
      .valueChanges()
      .subscribe(response => {
        this.firebaseComments = response;
      });
  }

  public ngOnInit(): void {
    this.firebaseData();
  }
}
