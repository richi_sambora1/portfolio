import { Sections } from '@pages/user-info/section';
import { ViewUserInfoComponent } from '@pages/user-info/view/v-user-info.component';

export const UserInfoComponents = [
  Sections,
  ViewUserInfoComponent,
];
