import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Comments } from '@shared/components/models/interfaces/comments';
import { FirebaseResponse } from '@shared/components/models/interfaces/firebaseResponse';
import { UserInfo } from '@shared/components/models/interfaces/user-Info';
import { MessageData } from '@shared/components/models/messages/messages.model';

@Component({
  selector: 's-user-info',
  templateUrl: './s-user-info.component.html',
  styleUrls: ['./s-user-info.component.scss'],
})
export class UserInfoComponent {
  @Input() public usersComments: Comments[];
  @Input() public MessageData: MessageData;
  @Input() public firebaseComments: FirebaseResponse[];
  @Input() public userInfo: UserInfo[];

  @Output() public addComment: EventEmitter<void> = new EventEmitter();
}
