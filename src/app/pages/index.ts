import { CareerComponents } from '@pages/career';
import { TechnologiesComponents } from '@pages/technologies';
import { UserInfoComponents } from '@pages/user-info';

export const Pages = [
  UserInfoComponents,
  CareerComponents,
  TechnologiesComponents,
];
