import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';

import { Pages } from '@pages/index';
import { SharedComponents } from '@shared/components';
import { AppMaterialModule } from './app-material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Services } from './services';
import { AnimationComponent } from './shared/animation/animation.component';


@NgModule({
  declarations: [
    AppComponent,
    Pages,
    SharedComponents,
    AnimationComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    FormsModule,
    HttpClientModule,
    AppMaterialModule,
    AppRoutingModule,
  ],
  providers: [Services],
  bootstrap: [AppComponent],
})
export class AppModule { }
