import { Injectable } from '@angular/core';

import { MessageData } from '@shared/components/models/messages/messages.model';

@Injectable({
  providedIn: 'root',
})
export class MessagesService {
  public messageData: MessageData = new MessageData();
}
