import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { WeatherResponse } from '@shared/components/models/interfaces/weatherResponse';

@Injectable({
  providedIn: 'root',
})
export class WeatherRequestService {
  public weatherUrl = 'http://api.openweathermap.org/data/2.5/weather?id=625144&appid=5a6dcec14ed7d9e5a3ac4ff94834d4e8&units=metric';

  constructor(
    public readonly http: HttpClient,
  ) { }

  public getTimeRequest$(): Observable<WeatherResponse> {
    return this.http.get<WeatherResponse>(this.weatherUrl)
      .pipe(
        map(
          (data) => {
            return data;
          }));
  }
}
