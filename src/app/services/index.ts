import { WeatherRequestService } from '@services/http-requests/weather-request/weather-request.service';
import { MessagesService } from '@services/messages/messages.service';

export const Services = [
  WeatherRequestService,
  MessagesService,
];
